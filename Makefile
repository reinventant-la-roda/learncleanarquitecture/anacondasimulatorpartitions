.PHONY: all run

all: clean build test

build:
	./gradlew assemble

run:
	java -jar build/libs/AnacondaPartition-*.jar

test:
	./gradlew build

clean:
	./gradlew clean

